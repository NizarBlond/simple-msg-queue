<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Database
    |--------------------------------------------------------------------------
    |
    | The database settings:
    | ----------------------
    |
    | 1. (string) connection: The connection name SMQ will use.
    | 2. (string) messages_table: The messages database table name.
    |
    */

    'db' => [
        'connection' => env('SMQ_DB_CONNECTION', 'mysql'),
        'messages_table' => env('SMQ_DB_MESSAGES_TABLE', 'smq_messages'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Default source
    |--------------------------------------------------------------------------
    |
    | Sets a default source for all messages.
    |
    */

    'default_source' => env('SMQ_DEFAULT_SOURCE', null),

    /*
    |--------------------------------------------------------------------------
    | Auto generate GUID
    |--------------------------------------------------------------------------
    |
    | Determines whether a message GUID must be generated automatically on
    | publishing a message.
    |
    */

    'auto_generate_guid' => env('SMQ_AUTO_GENERATE_GUID', true),

    /*
    |--------------------------------------------------------------------------
    | Auto expiry after publish
    |--------------------------------------------------------------------------
    |
    | Optionally sets an expiry time in minutes after the message is published.  
    |
    */

    'auto_expiry_after_mins' => env('SMQ_AUTO_EXPIRY_AFTER_MINS', null)
];
