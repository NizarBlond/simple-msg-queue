<?php

namespace NizarBlond\SimpleMsgQueue\Config;

use Exception;

class SimpleMsgQueue
{
    public static function dbConnection()
    {
        return self::config('db.connection');
    }

    public static function dbMessagesTableName()
    {
        return self::config('db.messages_table');
    }

    public static function defaultSource()
    {
        return self::config('default_source', true);
    }

    public static function autoGenerateGuid()
    {
        return self::config('auto_generate_guid', true);
    }

    public static function autoExpiryAfterMins()
    {
        return self::config('auto_expiry_after_mins', true);
    }

    public static function config($path, $allowEmpty = false)
    {
        $path   = "simple-msg-queue.$path";
        $config = config($path);
        if (! $allowEmpty && is_null($config)) {
            throw new Exception("Invalid config path '$path'.");
        }

        return $config;
    }
}
