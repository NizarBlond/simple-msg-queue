<?php

namespace NizarBlond\SimpleMsgQueue\Constants;

class MessageState
{
    /**
     * Processing status message.
     *
     * @var string
     */
    const NEW = "N";
    
    /**
     * Processing status message.
     *
     * @var string
     */
    const PROCESSING = "P";
    
    /**
     * Processing status message.
     *
     * @var string
     */
    const COMPLETED = "C";
    
}
