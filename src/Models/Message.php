<?php

namespace NizarBlond\SimpleMsgQueue\Models;

use Illuminate\Database\Eloquent\Model;
use NizarBlond\SimpleMsgQueue\Config\SimpleMsgQueue as SmqConfig;
use NizarBlond\SimpleMsgQueue\Constants\MessageState;
use Carbon\Carbon;

class Message extends Model
{
   /**
    * Whether timestamps exist.
    *
    * @var bool
    */
    public $timestamps = true;

   /**
    * The table name.
    *
    * @var string
    */
    protected $connection;

   /**
    * The casting array.
    *
    * @var array
    */
    protected $casts = [
        'context' => 'array'
    ];

   /**
    * The guarded fields.
    *
    * @var array
    */
    protected $guarded = [];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->connection = SmqConfig::dbConnection();
        $this->table = SmqConfig::dbMessagesTableName();

        parent::__construct($attributes);
    }
    
    /**
     * Scope a query to get messages by targets.
     *
     * @param   array $targets
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByTargets($query, $targets)
    {
        return $query->whereIn('target', $targets);
    }
    
    /**
     * Scope a query to get messages by targets.
     *
     * @param   array $types
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByTypes($query, $types)
    {
        return $query->whereIn('type', $types);
    }
    
    /**
     * Scope a query to get messages by state.
     *
     * @param   string  $state
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByState($query, $state)
    {
        return $query->where('state', $state);
    }

    /**
     * Scope a query to get exclude expired messages.
     *
     * @param   string  $state
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExcludeExpired($query)
    {
        return $query->where('expires_at', '>=', Carbon::now());
    }

    /**
     * Scope a query to get messages by days ago.
     *
     * @param string $days
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetFromDaysAgo($query, $days)
    {
        return $query->where('created_at', '>', Carbon::now()->subDays($days));
    }

    /**
     * Encodes content to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setContextAttribute($value)
    {
        $this->attributes['context'] = $this->convertToJson($value);
    }

    /**
     * Converts a given object/string to JSON string.
     *
     * @param   string|array  $value
     *
     * @return  string
     */
    protected function convertToJson($value)
    {
        if (empty($value)) {
            $value = [];
        }

        return is_array($value) ? json_encode($value) : $value;
    }

    /**
     * Updates message state.
     *
     * @param   string  $newState
     *
     * @return  void
     */
    public function setState($newState)
    {
        $this->state = $newState;
    }
}
