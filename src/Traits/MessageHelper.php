<?php

namespace NizarBlond\SimpleMsgQueue\Traits;

use NizarBlond\SimpleMsgQueue\Config\SimpleMsgQueue as SmqConfig;
use NizarBlond\SimpleMsgQueue\Constants\MessageState;
use NizarBlond\SimpleMsgQueue\Models\Message;
use Carbon\Carbon;
use Exception;

trait MessageHelper
{
    /**
     * Inserts a new message.
     *
     * @param   string  $type
     * @param   array   $context
     * @param   string  $target
     * @param   string  $source
     * @param   string  $state
     *
     * @return  \NizarBlond\SimpleMsgQueue\Models\Message
     */
    public static function publishMessage(
        $type,
        $context = [],
        $target = null,
        $source = null,
        $state = MessageState::NEW
    ) {
        return Message::create([
            'type' => $type,
            'source' => $source ?? SmqConfig::defaultSource(),
            'context' => $context,
            'target' => $target,
            'state' => $state,
            'guid' => SmqConfig::autoGenerateGuid()
                        ? bin2hex(openssl_random_pseudo_bytes(16))
                        : null,
            'expires_at' => self::getExpiryDate()
        ]);
    }

    /**
     * Returns messages by the specified target(s).
     *
     * @param   string|array    $targets
     * @param   string          $state
     *
     * @return  array
     */
    public static function readMessagesByTargets($targets, $state = "N")
    {
        $query = Message::getByTargets((array)$targets)->excludeExpired();

        return  (is_null($state)
                    ? $query
                    : $query->getByState($state)
                )->get()->toArray();
    }

    /**
     * Returns messages by the specified type(s).
     *
     * @param   string|array    $types
     * @param   string          $state
     *
     * @return  array
     */
    public static function readMessagesByTypes($types, $state = "N")
    {
        $query = Message::getByTypes((array)$types)->excludeExpired();

        return  (is_null($state)
                    ? $query
                    : $query->getByState($state)
                )->get()->toArray();
    }

    /**
     * Sets message state as completed.
     *
     * @param   string|integer  $id
     *
     * @return  void
     */
    public static function setMessageAsCompleted($id)
    {
        self::setMessageStateAs($id, MessageState::COMPLETED);
    }

    /**
     * Sets message state as processing.
     *
     * @param   string|integer  $id
     *
     * @return  void
     */
    public static function setMessageAsProcessing($id)
    {
        self::setMessageStateAs($id, MessageState::PROCESSING);
    }

    /**
     * Sets message state as specified.
     *
     * @param   string|integer  $types
     * @param   string          $newState
     *
     * @return  void
     */
    public static function setMessageStateAs($id, $newState)
    {
        $message = Message::find($id)->first();
        if (is_null($message)) {
            throw new Exception("Message was not found.");
        }

        $message->setState($newState);
        $message->save();
    }

    /**
     * Calculates the expiry date if any.
     *
     * @return static
     */
    private static function getExpiryDate()
    {
        $expireAfter = SmqConfig::autoExpiryAfterMins();
        if (empty($expireAfter)) {
            return null;
        }

        return Carbon::now()->addMinutes($expireAfter);
    }
}
