<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use NizarBlond\SimpleMsgQueue\Constants\MessageState;
use NizarBlond\SimpleMsgQueue\Config\SimpleMsgQueue as SmqConfig;

class CreateSmqMessagesTable extends Migration
{
    /**
     * The table name.
     *
     * @var string
     */
    private $tableName;

    /**
     * The constructor.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->tableName = SmqConfig::dbMessagesTableName();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection = SmqConfig::dbConnection();
        $schema = Schema::connection($connection);

        if ($schema->hasTable($this->tableName)) {
             return;
        }

        $schema->create($this->tableName, function ($table) {
            $table->increments('id');
            $table->string('guid')->nullable();
            $table->string('type');
            $table->string('source');
            $table->string('target')->nullable();
            $table->json('context')->nullable();
            $table->enum('state', [
                MessageState::NEW,
                MessageState::PROCESSING,
                MessageState::COMPLETED,
            ])->default(MessageState::NEW);
            $table->datetime('expires_at')->nullable();
            $table->timestamps();

            $table->index([ 'created_at', 'type', 'target' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
